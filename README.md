**PHP,JQuery AJAX, CSS**

**Install the App on Localhost Server**

## Download/Clone the app

Please follow the procedures below:

1. To download the app, Go to Source tab then click Clone and copy the provided URL.
2. Open terminal on your computer and navigate to your local host folder (ex. C:\xampp\htdocs\)
3. Paste the URL from the BitBucket to your terminal and hit enter.
4. Once done, proceed to setting up your local database.

## Database setup

Please follow the procedures below:

1. On your local database server (localhost/phpmyadmin), create new database with the name "file_uploading_app"
2. Import the (.sql) file from the downloaded app on this directory: simple-image-uploading-app\database\file_uploading_app.sql
3. Once successful, you can now visit the app on your browser. (localhost/simple-image-uploading-app)

Thank You!