<?php
	require './includes/requests.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>File Uploading App</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
<body>
	<div class="container mt-em">
		<div class="main">
			<!-- Section Header -->
			<section class="header-container">
				<h1 class="text-center">File Uploading App</h1>
				<h4 class="text-center">-- Coding Exam --</h4>
			</section>

			<!-- Section Body -->
			<section class="body-container">
				<div class="table-container">
					<button class="btn btn-primary" onclick="examJs.ShowModal(this, '#addModal', '')"><em class="fa fa-upload"></em> Upload Image</button>
					<table class="table table-striped" id="frmTable">
						<thead>
							<tr>
								<th class="text-left">Title</th>
								<th class="text-center">Thumbnail</th>
								<th class="text-center">Filename</th>
								<th class="text-center">Date added</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if ($results && !empty($results)):
								foreach ($results as $data): ?>
								<tr>
									<td class="text-left pd-l4">
										<?php echo $data->title; ?>
									</td>
									<td class="text-center">
										<img src="assets/uploads/<?php echo $data->filename; ?>" class="img-thumbnail">
									</td>
									<td class="text-center">
										<?php 
											$fileName = substr($data->filename, strpos($data->filename, "-") + 1);
										?>
										<a href="assets/uploads/<?php echo $data->filename; ?>" download="<?php echo $data->filename ?>">
											<?php echo $fileName; ?>
										</a>
									</td>
									<td class="text-center">
										<?php echo date('M. j,Y', strtotime($data->created_at)); ?>
									</td>
									<td class="text-center">
										<button class="btn btn-info btn-sm" onclick="examJs.ModalUpdate('includes/requests.php', 'POST', '#editModal', '<?php echo $data->id ?>')"><em class="fa fa-edit"></em> Edit</button>
										<button class="btn btn-danger btn-sm" onclick="examJs.ShowModal(this, '#deleteModal', '<?php echo $data->id ?>')"><em class="fa fa-trash"></em> Delete</button>
									</td>
								</tr>
							<?php endforeach; 
							else: ?>
								<tr>
									<td colspan="5">
										<p class="alert alert-info">No records found!</p>
									</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</section>
			<div class="spacer"></div>

			<!-- Section Footer -->
			<section class="footer-container">
				<div class="footer-l">
					<p class="text-center">Started: Nov. 27, 2021, 2PM - Completed: Nov. 28, 2021, 9PM</p>
					<p class="text-center">
						<small>Technology used: PHP|CSS|JS/JQuery|AJAX|MySQL|HTML5</small>
					</p>
					<p class="text-center">
						<small>Developer: <strong>Jeric Rodriguez</strong> | rodriguezjeric@gmail.com</small>
					</p>
				</div>
			</section>

		</div>
	</div>

	<?php include_once('includes/modals.php') ?>

	<!-- JQuery For Ajax -->
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<!-- Icons -->
	<script src="https://use.fontawesome.com/f51b6c3196.js"></script>
	<!-- Main JS -->
	<script type="text/javascript" src="assets/js/main.js"></script>
	
	<script type="text/javascript">

		// Add
		$('.submitProcessAddBtn').click(function() {
			var frmId = '#'+$(this).parents().find('#addModal form').attr('id');
			var modalId = '#'+$(this).parents().find('#modalParentAdd').attr('id');

			if (examJs.ValidateForm(frmId, true)) {
				isLoadingBtn(true, '.submitProcessBtn', 'Loading...');

				var formData = new FormData($(frmId)[0]);

				// call ajax add new function
				examJs.Add('includes/requests.php', 'POST', formData, modalId);
				examJs.ReloadDataTable('index.php', '#frmTable', 500);
				examJs.FormReset(frmId);

				isLoadingBtn(false, '.submitProcessBtn', 'Submit');
			}
		});

		// Delete
		$('.submitProcessDeleteBtn').click(function() {
			var frmId = '#'+$(this).parents().find('#deleteModal form').attr('id');
			var modalId = '#'+$(this).parents().find('#modalParentDelete').attr('id');

			isLoadingBtn(true, '.submitProcessDeleteBtn', 'Loading...');

			var formData = new FormData($(frmId)[0]);
			// call ajax add new function
			examJs.Delete('includes/requests.php', 'POST', formData, modalId);
			examJs.ReloadDataTable('index.php', '#frmTable', 500);

			isLoadingBtn(false, '.submitProcessDeleteBtn', 'Submit');
		});

		// Edit
		$('.submitProcessEditBtn').click(function() {
			var frmId = '#'+$(this).parents().find('#editModal form').attr('id');
			var modalId = '#'+$(this).parents().find('#modalParentEdit').attr('id');
			
			if (examJs.ValidateForm(frmId, false)) {
				isLoadingBtn(true, '.submitProcessEditBtn', 'Loading...');

				var formData = new FormData($(frmId)[0]);
				// call ajax add new function
				examJs.Edit('includes/requests.php', 'POST', formData, modalId);
				examJs.ReloadDataTable('index.php', '#frmTable', 500);
				examJs.FormReset(frmId);

				isLoadingBtn(false, '.submitProcessEditBtn', 'Update');
			}
		});

		function isLoadingBtn(status, btnId, text) {
			if (status) {
				$(btnId).attr('disabled', true).html('').append('<em class="fa fa-spin fa-spinner"></em> '+text);
			} else {
				$(btnId).attr('disabled', false).html('').append(text);
			}
			
			return true;
		}

		examJs.ResetInputsOnFocus();
	</script>
</body>
</html>