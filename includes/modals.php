<!-- Modals -->
	<!-- Add Modal -->
	<div class="modal modal-container" id="modalParentAdd">
		<div class="modal-cont" id="addModal">
			<div class="modal-header">
				<p id="title">Upload New Image</p>
				<span id="closeModal" onclick="examJs.HideModal(this, '#addModal')" class="close-modal">x</span>
			</div>
			<div class="modal-body">
				<form method="post" enctype="multipart/form-data" id="addForm">
					<input type="hidden" name="action" value="add">
					<div class="input-container">
						<label for="fileTitle">Title</label>
						<input type="text" name="fileTitle" id="fileTitle" class="frmInput" required="">
					</div>
					<div class="input-container">
						<label for="fileData">Image</label>
						<input type="file" accept="image/*" name="fileData" id="fileData" class="frmInput file" required="">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="input-container">
					<button type="button" class="btn btn-success btn-block btn-md submitProcessAddBtn" id="submitProcessBtn">Submit</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Modal -->
	<div class="modal modal-container" id="modalParentDelete">
		<div class="modal-cont" id="deleteModal">
			<div class="modal-header">
				<p id="title">Confirm Deletion</p>
				<span id="closeModal" onclick="examJs.HideModal(this, '#deleteModal')" class="close-modal">x</span>
			</div>
			<div class="modal-body">
				<form method="post" id="deleteForm">
					<input type="hidden" name="dataId" class="dataId">
					<input type="hidden" name="action" value="delete">
					<div class="input-container">
						<p class="alert alert-danger">Are you sure you want to delete this image and its info?</p>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="input-container">
					<button type="button" class="btn btn-danger btn-block btn-md submitProcessDeleteBtn">Delete</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Edit Modal -->
	<div class="modal modal-container" id="modalParentEdit">
		<div class="modal-cont" id="editModal">
			<div class="modal-header">
				<p id="title">Update Image Info</p>
				<span id="closeModal" onclick="examJs.HideModal(this, '#editModal')" class="close-modal">x</span>
			</div>
			<div class="modal-body">
				<form method="post" id="editForm">
					<input type="hidden" name="dataId" class="dataId">
					<input type="hidden" name="currentFileName" class="currentFileName">
					<input type="hidden" name="action" value="edit">
					<div class="img-cont text-center">
						<img src="" class="img-thumbnail-2">
					</div>
					<div class="input-container">
						<label for="editFileTitle">Title</label>
						<input type="text" name="editFileTitle" id="editFileTitle" class="frmInput">
					</div>
					<div class="input-container">
						<label for="editFileData">Image</label>
						<input type="file" accept="image/*" name="editFileData" id="editFileData" class="frmInput file">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="input-container">
					<button type="button" class="btn btn-success btn-block btn-md submitProcessEditBtn">Update</button>
				</div>
			</div>
		</div>
	</div>
<!-- End of Modals -->