<?php
include_once('Controller/ImageController.class.php');
$img = new ImageController();
$results = $img->getAllData();

if (isset($_POST) && !empty($_POST['action'])):

	switch ($_POST['action']) {
		case 'add':
			$newFileName = rand(100, 1000).'-'.$_FILES['fileData']['name'];
			$params = array('title' => $_POST['fileTitle'], 'fileName' => $newFileName);
			//Get the temp file path
			$tmpFilePath = $_FILES['fileData']['tmp_name'];

			if ($tmpFilePath != ""){
			    $newFilePath = "../assets/uploads/".$newFileName;

			    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
			    	if ($img->addImage($params)) {
						$result['status'] = 'OK';
					}
			    }
			}
			break;

		case 'edit':

			if ($_FILES['editFileData']['name'] != '') {
				$newFileName = rand(100, 1000).'-'.$_FILES['editFileData']['name'];
				$params = array('title' => $_POST['editFileTitle'], 'fileName' => $newFileName, 'dataId' => $_POST['dataId']);
				//Get the temp file path
				$tmpFilePath = $_FILES['editFileData']['tmp_name'];

				if ($tmpFilePath != ""){
				    $newFilePath = "../assets/uploads/".$newFileName;

				    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
				    	if ($img->editImage($params)) {
							$result['status'] = 'OK';
						}
				    }
				}
			} else {
				$params = array('title' => $_POST['editFileTitle'], 'fileName' => $_POST['currentFileName'], 'dataId' => $_POST['dataId']);
				if ($img->editImage($params)) {
					$result['status'] = 'OK';
				}
			}
			
			break;

		case 'delete':
			$dataId = $_POST['dataId'];
			if ($dataId && !empty($dataId)) {
				if ($img->deleteImage($dataId)) {
					$result['status'] = 'OK';
				}
			}
			break;

		case 'get-data':
			$dataId = $_POST['dataId'];
			if ($dataId && !empty($dataId)) {
				if ($result = $img->getOne($dataId)) {
					$result['result'] = $result;
					$result['status'] = 'OK';
				}
			}
			break;
		
		default:
			# code...
			break;
	}

	echo json_encode($result);
	exit;

endif;