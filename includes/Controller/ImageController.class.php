<?php
include_once('DatabaseController.class.php');
// Process File Class
class ImageController
{	
	private $db;

	public function __construct() {
		$this->db = new DatabaseConnection();
	}

	public function getAllData() {
		$sql = "SELECT * FROM fua_files ORDER BY id DESC";
	    $query = $this->db->prepare($sql);
	    $query->execute();
	    $result = $query->fetchAll(PDO::FETCH_OBJ);

	    return $result;
	}

	public function addImage($request = array()) {
		if (!$request && empty($request)) return false;

		$request = array($request['title'], $request['fileName']);
		$sql = "INSERT INTO fua_files (title, filename) VALUES (?,?)";
        $query = $this->db->prepare($sql);
        $query->execute($request);

        return true;
	}

	public function editImage($request = array()) {
		if (!$request && empty($request)) return false;

		$request = array('title' => $request['title'], 'filename' => $request['fileName'], 'updated_at' => date('Y-m-d'), 'dataId' => $request['dataId']);
		$sql = "UPDATE fua_files SET title=:title, filename=:filename, updated_at=:updated_at WHERE id=:dataId";
        $query = $this->db->prepare($sql);
        $query->execute($request);

        return true;
	}

	public function deleteImage($id) {
		if (!$id && empty($id)) return false;

		$sql = "DELETE FROM fua_files WHERE id=:dataId";
        $query = $this->db->prepare($sql);
        $query->execute(['dataId'=>$id]);

        return true;
	}

	public function getOne($id) {
		if (!$id && empty($id)) return false;

		$sql = "SELECT * FROM fua_files WHERE id=:dataId";
        $query = $this->db->prepare($sql);
        $query->execute(['dataId'=>$id]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
	}
}