<?php

class DatabaseConnection extends PDO
{
    private $host       = "127.0.0.1";
    private $dbname     = "file_uploading_app";
    private $dbuser 	= "root";
    private $dbpass     = "";

    public function __construct(){
        try{
            parent::__construct("mysql:host=$this->host;dbname=$this->dbname", $this->dbuser, $this->dbpass);
	        $this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	        $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
        catch (PDOException $e){
            $e->getMessage();
        }
    }
}