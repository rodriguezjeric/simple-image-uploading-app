const examJs = new examJsClass();

function examJsClass(){
//    code here...
}

examJsClass.prototype.ShowModal = function(el, id, dataId) {
	if (dataId != '') {
		$(".dataId").val(dataId);
	}
    $(id).parent().show();
}

examJsClass.prototype.HideModal = function(el, id) {
    $(id).parent().hide();
}

examJsClass.prototype.Add = function(url, type, data, modalId) {
	if (!data) return false;

	$.ajax({
		url: url,
		type: type,
		data: data,
		processData: false,
        contentType: false,
		success: function(data) {
			var results = JSON.parse(data);
			if (results.status == 'OK') {
				$(modalId).hide();
				notificationPopUp('success', 'New image successfully added!');
			}
		}, error: function(err) {
			console.log(err);
			return false;
		}
	});
}

examJsClass.prototype.Delete = function(url, type, data, modalId) {
	if (!data) return false;

	$.ajax({
		url: url,
		type: type,
		data: data,
		processData: false,
        contentType: false,
		success: function(data) {
			var results = JSON.parse(data);
			if (results.status == 'OK') {
				$(modalId).hide();
				notificationPopUp('success', 'Image info successfully deleted!');
			}
		}, error: function(err) {
			console.log(err);
			return false;
		}
	});
}

examJsClass.prototype.Edit = function(url, type, data, modalId) {
	if (!data) return false;

	$.ajax({
		url: url,
		type: type,
		data: data,
		processData: false,
        contentType: false,
		success: function(data) {
			var results = JSON.parse(data);
			if (results.status == 'OK') {
				$(modalId).hide();
				notificationPopUp('success', 'Image info successfully updated!');
			}
		}, error: function(err) {
			console.log(err);
			return false;
		}
	});
}

examJsClass.prototype.ValidateForm = function(formId, validateFile) {
	if (!formId) return false;
	var errors = 0;
	if (validateFile) {
		$(formId+" :input").each(function() {
			var input = $(this);
			if (input.val() == '') {
				$(input).css('border', '1px solid red');
				$(input).parent().find('small').remove();
				$(input).parent().append('<small class="text-danger text-left">This field is required!</small>');
				errors++;
			}
		});
	} else {
		$(formId+" :input[type='text']").each(function() {
			var input = $(this);
			if (input.val() == '') {
				$(input).css('border', '1px solid red');
				$(input).parent().find('small').remove();
				$(input).parent().append('<small class="text-danger text-left">This field is required!</small>');
				errors++;
			}
		});
	}

	return errors == 0 ? true : false;
}

examJsClass.prototype.ResetInputsOnFocus = function() {
	$('input').on('focus', function() {
		$(this).css('border', '1px solid #c0c0c0');
		$(this).parent().find('small').remove();
	});
}

examJsClass.prototype.ReloadDataTable = function(file, tableId, timeout) {
	setTimeout(function(){
	   $(tableId).load(file+" "+ tableId);
	}, timeout);
}

examJsClass.prototype.FormReset = function(formId) {
	$(formId)[0].reset();
}

examJsClass.prototype.ModalUpdate = function(url, type, modalId, dataId) {
	if (!dataId && !modalId && !fileName) return false;

	$.ajax({
		url: url,
		type: type,
		data: {'dataId': dataId, 'action': 'get-data'},
		success: function(data) {
			var results = JSON.parse(data);
			if (results.status == 'OK') {
				examJs.ShowModal('', modalId, dataId);
				$(modalId).find("input[name='editFileTitle']").val(results.result.title);
				$(modalId).find("input[name='currentFileName']").val(results.result.filename);
				$(modalId).find("img").attr('src', 'assets/uploads/'+results.result.filename);
			}
		}, error: function(err) {
			console.log(err);
			return false;
		}
	});
}

function notificationPopUp(type, msg) {
	if (!type && !msg) return false;

	let appendHtmlMsg = '';
	switch (type) {
		case 'success':
			appendHtmlMsg = '<div class="popup msg-success">'
								+'<p>'+msg+'</p>'
							+'</div>';
			break;
		case 'warning':
			appendHtmlMsg = '<div class="popup msg-warning">'
								+'<p>'+msg+'</p>'
							+'</div>';
			break;
		case 'danger':
			appendHtmlMsg = '<div class="popup msg-danger">'
								+'<p>'+msg+'</p>'
							+'</div>';
			break;
		case 'info':
			appendHtmlMsg = '<div class="popup msg-info">'
								+'<p>'+msg+'</p>'
							+'</div>';
			break;
		default:
			return false;
			break;
	}

	$('body').append(appendHtmlMsg).fadeIn('fast');
    setTimeout(function(){
        $('.popup').fadeOut('slow');
    },4000);
    setTimeout(function(){
        $('.popup').remove();
    },4000);
}